const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const wait = require('gulp-wait');
const minify = require('gulp-minify');
const cleanCss = require('gulp-clean-css');
const htmlreplace = require('gulp-html-replace');
const rename = require("gulp-rename");
const clean = require('gulp-clean');
const gulpif = require('gulp-if');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');
const imagemin = require('gulp-imagemin');
const runSeq = require('run-sequence');

// Create CSS from SASS
gulp.task('sass', () => {
  	return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss','src/scss/*.scss'])
    .pipe(sass())
    .pipe(gulp.dest('src/css'))
    .pipe(wait(500))
    .pipe(browserSync.stream());
});

// ES6 conversion and bundle all JS into one
gulp.task('convert-js', function(){
  browserify('./src/js/modules/main.js')
  .transform(babelify, {presets: ['@babel/preset-env']})
  .bundle()
  .pipe(source('bundle.js'))
  .pipe(gulp.dest('./src/js'))
  .pipe(wait(500))
  .pipe(browserSync.stream());
});

// Watching scss and html files
gulp.task('serve', ['sass', 'convert-js'], () => {
  	browserSync.init({
    	server: "./src"
  	});

  	gulp.watch(['node_modules/bootstrap/scss/*.scss', 'src/scss/*.scss'], ['sass']);
    gulp.watch(['src/js/modules/*.js'], ['convert-js']);
  	gulp.watch("src/*.html").on('change', browserSync.reload);
});

// Minify and move JS files to DIST
gulp.task('min-js', () => {
    return gulp.src(['src/js/*.js'])
    .pipe(gulpif('bundle.js', minify({noSource: true})))
    .pipe(gulp.dest('dist/js'));
});

// Rename JS file in DIST (before rename first minify)
gulp.task('ren-js', ['min-js'], () => {
	gulp.src('dist/js/bundle-min.js')
	.pipe(gulpif('bundle-min.js', rename('bundle.min.js')))
	.pipe(gulp.dest('dist/js'));
});

// Remove old JS file
gulp.task('remove-js', () => {
  return gulp.src('dist/js/bundle-min.js', {read: false})
    .pipe(clean());
});

// Minify and move CSS files to DIST
gulp.task('min-css', () => {
    return gulp.src(['src/css/*.css'])
    .pipe(cleanCss())
   	.pipe(gulp.dest('dist/css'));
});

// Rename CSS file in DIST (before rename first minify)
gulp.task('ren-css', ['min-css'], () => {
	gulp.src('dist/css/*.css')
	.pipe(gulpif('bootstrap.css', rename('bootstrap.min.css')))
	.pipe(gulpif('style.css', rename('style.min.css')))
  .pipe(gulpif('gallery-grid.css', rename('gallery-grid.min.css')))
	.pipe(gulp.dest('dist/css'));
});

// Remove old CSS file
gulp.task('remove-css', () => {
  return gulp.src(['dist/css/bootstrap.css', 'dist/css/style.css', 'dist/css/gallery-grid.css'], {read: false})
  .pipe(clean());
});

// Minify HTML files
gulp.task('min-html', () => {
  	return gulp.src('src/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'));
});

// Replace CSS and JS names to minimized names
gulp.task('rep-names', ['min-html'], () => {
  	gulp.src('dist/*.html')
    .pipe(gulpif('gallery.html', htmlreplace({
            'css': ['css/style.min.css', 'css/bootstrap.min.css', 'css/gallery-grid.min.css'],
            'js': ['js/bundle.min.js', 'js/bootstrap.min.js']
    })))
    .pipe(gulpif('!gallery.html', htmlreplace({
            'css': ['css/style.min.css', 'css/bootstrap.min.css'],
            'js': ['js/bundle.min.js', 'js/bootstrap.min.js']
    })))
    .pipe(gulp.dest('dist'));
});

// Move IMG to DIST
gulp.task('move-img', () => {
    gulp.src(['src/img/*', '!src/img/psd'])
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
});

gulp.task('build-dist', () => {
  return runSeq('sass', 'convert-js', 'ren-js', 'ren-css', 'rep-names', 'move-img', 'remove-js', 'remove-css');
});

gulp.task('default', ['convert-js','serve']);