const serviceContent = document.querySelector('.service-content');
const markup = `<div class="container">
        			<header class="row">
          				<div class="col-lg-12">
            				<h1 class="pg-title headline text-center">%title%</h1>
          				</div>
        			</header>
        			<div class="row">
          				<div class="col-md-6">
            				<img src="%img%" class="shadow-lg">
          				</div>
          				<article class="col-md-6 mt-5 mt-md-0">
            				<p>%content%</p>
          				</article>
        			</div>        
      			</div>`;

const editHTML = (ttl, img, cont) => {
  let newHtml;

  newHtml = markup.replace('%title%', ttl);
  newHtml = newHtml.replace('%img%', img);
  newHtml = newHtml.replace('%content%', cont);

  serviceContent.insertAdjacentHTML('beforeend', newHtml);
}

export const renderService = () => {

  // Get service name from URL

  const id = window.location.hash.replace('#','');

  // Clear all content from SERVICE page

  serviceContent.innerHTML = '';

  const serviceElements = function(title, image, content){
    this.title = title;
    this.image = image;
    this.content = content;
  }

  let newElement;

  // Render service page if an ID exists

  if(id){
    switch(id){
      case 'ser1':
        newElement = new serviceElements('Restaurativna dentalna medicina', 'img/restaurativna_medicina.jpg', 'Dentalna protetika podrazumijeva nadomještanje izgubljenih zuba i mekih tkiva usne šupljine. Koncept dentalne protetike kreće se dvama smjerovima – mobilnom i fiksnom protetikom. Mobilna protetika temelji se na izradi nadomjestaka koji se mogu skidati (radi čišćenja), dok se jednom postavljeni fiksni nadomjesci više ne mogu skinuti. Kombinacija fiksnog i mobilnog rada naziva se kombinirani rad kod kojega je jedan dio mobilan te pričvršćen veznim elementima za fiksni dio cementiran na zube u ustima. Zajedno čine cjelinu koja služi žvakanju čvrsto stojeći u ustima. Mobilna protetika podrazumijeva proteze, odnosno nadomjestak dijela ili cijele denticije koja se vadi iz usta radi čišćenja. Fiksna protetika podrazumijeva krunice i most. Krunica ili navlaka ima funkciju očuvanja zubne krune kod zuba oštećenih karijesom ili lomom, a u slučajevima kada zubna ispuna nije dovoljna. Osim toga, ima i estetsku funkciju zbog toga što može korigirati boju, poziciju i rotaciju postojećeg zuba izrazito estetskim materijalom (keramikom). Most jest nadomjestak jednog ili više zuba prilikom čega se bruse susjedni zubi koji preuzimaju ulogu nosača međučlanova nadomještajući zube koji nedostaju.');
      
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser2':
        newElement = new serviceElements('Estetska dentalna medicina', 'img/estetska_medicina.jpg', 'Estetska dentalna medicina smjer je dentalne medicine koji primarno prednost daje estetici, no to nikako nije na štetu funkcije zuba (čvrstoća, trajnost, mogućnost žvakanja i slično). Podrazumijeva materijale koji u vrlo visokoj točnosti oponašaju boju i oblik prirodnih zuba. Dio estetske dentalne medicine su i djelomične keramičke krunice (ljuskice) koje pokrivaju vidljivi dio zuba uz minimalno brušenje, kao i potpune keramičke krunice koje u cijelosti oponašaju izgled prirodnog zuba odlično prianjajući uz zubno meso i ne ostavljajući nikakav rub na njemu.');
      
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser3':
        newElement = new serviceElements('Dječja dentalna medicina', 'img/djecja_dentalna_medicina.jpg', 'Dječja dentalna medicina obuhvaća djecu do 18 godina starosti. Preporučamo da prvi posjet bude već oko druge godine starosti, kako bi vaše dijete, osim kontrole zuba, bilo u mogućnosti izgraditi potpuno povjerenje u okruženje i našeg liječnika dentalne medicine. Vrlo je važno da djeca taj posjet prihvate kao igru te na taj način od najranije dobi usvoje pozitivan stav prema oralnoj higijeni.');

        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser4':
        newElement = new serviceElements('Endodoncija', 'img/endodoncija.jpg', 'Endodoncija je grana dentalne medicine koja se bavi unutrašnjošću zuba, odnosno prostorom u kojem se nalazi zubni živac. Uzrok upale zubnog živca može biti lom krune zuba, veliki zubni ispun ili prodor karijesa do granice iritacije. Najčešći simptom je jak bol tijekom noći, no on može biti i slabiji ili u potpunosti izostati. Odumiranjem živca i njegovim postupnim raspadanjem, gomilaju se bakterije koje mogu infekciju prenijeti na kost i okolna tkiva. Svakim zanemarivanjem ovih simptoma liječenje postaje zahtjevnije, a samim time i dugotrajnije. Cilj endodontskih zahvata jest uklanjanje unutarnjeg tkiva, dezinfekcija inficiranog prostora te hermetičko ispunjavanje unutrašnjosti zuba kako bi se zatvorio put postojećim bakterijama. U našoj ordinaciji dentalne medicine radi se strojna obrada korijenskih kanala koja daje izvrsne rezultate te omogućava znatno kraći tretman uz potpunu bezbolnost.');
      
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser5':
        newElement = new serviceElements('Dentalna protetika', 'img/dentalna_protetika.jpg', 'Dentalna protetika podrazumijeva nadomještanje izgubljenih zuba i mekih tkiva usne šupljine. Koncept dentalne protetike kreće se dvama smjerovima – mobilnom i fiksnom protetikom. Mobilna protetika temelji se na izradi nadomjestaka koji se mogu skidati (radi čišćenja), dok se jednom postavljeni fiksni nadomjesci više ne mogu skinuti. Kombinacija fiksnog i mobilnog rada naziva se kombinirani rad kod kojega je jedan dio mobilan te pričvršćen veznim elementima za fiksni dio cementiran na zube u ustima. Zajedno čine cjelinu koja služi žvakanju čvrsto stojeći u ustima. Mobilna protetika podrazumijeva proteze, odnosno nadomjestak dijela ili cijele denticije koja se vadi iz usta radi čišćenja. Fiksna protetika podrazumijeva krunice i most. Krunica ili navlaka ima funkciju očuvanja zubne krune kod zuba oštećenih karijesom ili lomom, a u slučajevima kada zubna ispuna nije dovoljna. Osim toga, ima i estetsku funkciju zbog toga što može korigirati boju, poziciju i rotaciju postojećeg zuba izrazito estetskim materijalom (keramikom). Most jest nadomjestak jednog ili više zuba prilikom čega se bruse susjedni zubi koji preuzimaju ulogu nosača međučlanova nadomještajući zube koji nedostaju.');
      
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser6':
        newElement = new serviceElements('Oralna kirurgija', 'img/oralna_kirurgija.jpg', 'U našoj se ordinaciji dentalne medicine izvode sljedeći oralno kirurški postupci: vađenje zuba, obrada koštanog grebena, oblikovanje zubnog mesa te ugradnja implantata. Prije vađenja zuba, obavljaju se sve pripremne radnje (kontrola premedikacije), a samo vađenje radi se u stanju potpune bezbolnosti. Nakon što je zub izvađen obavljaju se sve potrebne radnje (ukoliko je potrebno šivanje, antibiotik ili nešto drugo). U ovoj se ordinaciji svi kirurški zahvati obavljaju pod strogo kontroliranim uvjetima sterilizacije.');

        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser7':
        newElement = new serviceElements('Parodontologija', 'img/parodontologija.jpg', 'Parodontologija je grana dentalne medicine koja se bavi liječenjem parodontnih tkiva koje okružuju zube i omogućuju njihovu stabilnost u zubnom luku. Njezin su glavni predmet zubno meso (gingiva), sveza koja drži zub u kosti (parodontni ligament) te kost. Općepoznati izraz u narodu za bolest ispadanja zdravih zuba jest parodontoza (označava kronični parodontitis). Ona započinje upalom zubnog mesa (gingvitisom) koji se očituje njegovim krvarenjem, a slijedi povlačenje potpornog tkiva zuba s korijena zuba te njihova sveza popušta. Parodontoza je najčešće uzrokovana manjkom oralne higijene te nakupljenim kamencem. Zahvati koji se izvode u našoj ordinaciji su: čišćenje svih naslaga ultrazvučnim instrumentom (skidanje kamenca), pjeskarenje zuba, poliranje zuba, tretiranje kemijskim sredstvima, struganje i poliranje zubnih korijena te uklanjanje viška zubnog mesa (gingivektomija). Pri prvim znakovima koji upućuju na parodontozu izrazito je važno obaviti pregled kako bi se ona na vrijeme počela liječiti. Obavezno je pridržavati se redovitih propisanih kontrola svakih tri do šest mjeseci kako se stanje ne bi pogoršalo ili vratilo.');
      
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser8':
        newElement = new serviceElements('Rendgen', 'img/rendgen.jpg', 'Rendgenski je snimak danas standardno sredstvo dentalne medicine, kako u dijagnostici, tako i u kontroli terapije. U našoj se ordinaciji koristi digitalni rendgen čija su zračenja minimalna, a pacijent je prilikom snimanja prekriven zaštitnom olovnom pregačom. Rezultate intraoralnog i retroalveolarnih snimaka moguće je primiti u digitalnom obliku.');

        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser9':
        newElement = new serviceElements('Ortopan', 'img/ortopan.jpg', 'Ortopan je snimka koja reproducira cjelokupnu maksilofacijalnu regiju, uključujući i temporomandibularne zglobove (TMZ). Snimka može biti i u digitalnom formatu na CD (digitalni ortopan) za prikazivanje na zaslonu računala. Svi ortopani rade se digitalnom CCD tehnologijom i imaju visoku preciznost svih prikazanih struktura što omogućuje uočavanje više detalja i bolju dijagnostičku primjenu. Optimalna geometrija snimanja omogućuju precizan i neiskrivljen prikaz zuba gornje i donje čeljusti. Jasno se uočavaju patološki procesi oko svih korijena zuba, stanje kosti u slučajevima parodontitisa te karijesne lezije na zubima. Dijagnostički najvažnija snimka za pravovremeno otkrivanje bolesti zuba i okolnih struktura.');
      
        editHTML(newElement.title, newElement.image, newElement.content);
        break;
    }
  }
}