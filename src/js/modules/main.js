import { renderService } from './serviceView';
import { sendMail } from './mailForm';

$(() => {

   	$('.banner-cont .banner-title h1').show('slide',{direction: 'left'}, 2000);
   	
   	if (window.location.href.indexOf("service.html") > -1) {
   		renderService();
   	}
   	
   	sendMail();
   
 });