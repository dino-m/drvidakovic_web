import * as emailjs from './email.min';

export const sendMail = () => {
	const contForm = document.getElementById('contact-form'),
   		  name = document.getElementById('nameInput'),
   		  email = document.getElementById('emailInput'),
   		  phone = document.getElementById('telInput'),
   		  content = document.getElementById('contentInput');

	emailjs.init("user_PazhDT15rotdDZGc7UbQE");
	
	contForm.addEventListener('submit', (e) => {
		e.preventDefault();
		
		if(!name.value || !email.value || !phone.value || !content.value){
			document.querySelector('.send-msg p:nth-child(1)').style.display = 'none';
			document.querySelector('.send-msg p:nth-child(2)').style.display = 'block';
		}else {
			emailjs.sendForm('gmail', 'vidakovic', contForm);
			document.querySelector('.send-msg p:nth-child(2)').style.display = 'none';
			document.querySelector('.send-msg p:nth-child(1)').style.display = 'block';
			
			name.value="";
			email.value="";
			phone.value="";
			content.value="";
			name.focus();
		}
	});
}