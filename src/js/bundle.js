(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function (global){
"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (e) {
  if ("object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module) module.exports = e();else if ("function" == typeof define && define.amd) define([], e);else {
    ("undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this).emailjs = e();
  }
}(function () {
  return function i(u, s, c) {
    function a(t, e) {
      if (!s[t]) {
        if (!u[t]) {
          var n = "function" == typeof require && require;
          if (!e && n) return n(t, !0);
          if (l) return l(t, !0);
          var r = new Error("Cannot find module '" + t + "'");
          throw r.code = "MODULE_NOT_FOUND", r;
        }

        var o = s[t] = {
          exports: {}
        };
        u[t][0].call(o.exports, function (e) {
          return a(u[t][1][e] || e);
        }, o, o.exports, i, u, s, c);
      }

      return s[t].exports;
    }

    for (var l = "function" == typeof require && require, e = 0; e < c.length; e++) {
      a(c[e]);
    }

    return a;
  }({
    1: [function (e, t, n) {
      var r,
          o,
          i = t.exports = {};

      function u() {
        throw new Error("setTimeout has not been defined");
      }

      function s() {
        throw new Error("clearTimeout has not been defined");
      }

      function c(t) {
        if (r === setTimeout) return setTimeout(t, 0);
        if ((r === u || !r) && setTimeout) return r = setTimeout, setTimeout(t, 0);

        try {
          return r(t, 0);
        } catch (e) {
          try {
            return r.call(null, t, 0);
          } catch (e) {
            return r.call(this, t, 0);
          }
        }
      }

      !function () {
        try {
          r = "function" == typeof setTimeout ? setTimeout : u;
        } catch (e) {
          r = u;
        }

        try {
          o = "function" == typeof clearTimeout ? clearTimeout : s;
        } catch (e) {
          o = s;
        }
      }();
      var a,
          l = [],
          f = !1,
          d = -1;

      function p() {
        f && a && (f = !1, a.length ? l = a.concat(l) : d = -1, l.length && m());
      }

      function m() {
        if (!f) {
          var e = c(p);
          f = !0;

          for (var t = l.length; t;) {
            for (a = l, l = []; ++d < t;) {
              a && a[d].run();
            }

            d = -1, t = l.length;
          }

          a = null, f = !1, function (t) {
            if (o === clearTimeout) return clearTimeout(t);
            if ((o === s || !o) && clearTimeout) return o = clearTimeout, clearTimeout(t);

            try {
              o(t);
            } catch (e) {
              try {
                return o.call(null, t);
              } catch (e) {
                return o.call(this, t);
              }
            }
          }(e);
        }
      }

      function h(e, t) {
        this.fun = e, this.array = t;
      }

      function v() {}

      i.nextTick = function (e) {
        var t = new Array(arguments.length - 1);
        if (1 < arguments.length) for (var n = 1; n < arguments.length; n++) {
          t[n - 1] = arguments[n];
        }
        l.push(new h(e, t)), 1 !== l.length || f || c(m);
      }, h.prototype.run = function () {
        this.fun.apply(null, this.array);
      }, i.title = "browser", i.browser = !0, i.env = {}, i.argv = [], i.version = "", i.versions = {}, i.on = v, i.addListener = v, i.once = v, i.off = v, i.removeListener = v, i.removeAllListeners = v, i.emit = v, i.prependListener = v, i.prependOnceListener = v, i.listeners = function (e) {
        return [];
      }, i.binding = function (e) {
        throw new Error("process.binding is not supported");
      }, i.cwd = function () {
        return "/";
      }, i.chdir = function (e) {
        throw new Error("process.chdir is not supported");
      }, i.umask = function () {
        return 0;
      };
    }, {}],
    2: [function (e, t, n) {
      (function (f, d) {
        (function () {
          "use strict";

          function e(t) {
            var n = this.constructor;
            return this.then(function (e) {
              return n.resolve(t()).then(function () {
                return e;
              });
            }, function (e) {
              return n.resolve(t()).then(function () {
                return n.reject(e);
              });
            });
          }

          var t = setTimeout;

          function r() {}

          function i(e) {
            if (!(this instanceof i)) throw new TypeError("Promises must be constructed via new");
            if ("function" != typeof e) throw new TypeError("not a function");
            this._state = 0, this._handled = !1, this._value = void 0, this._deferreds = [], l(e, this);
          }

          function o(n, r) {
            for (; 3 === n._state;) {
              n = n._value;
            }

            0 !== n._state ? (n._handled = !0, i._immediateFn(function () {
              var e = 1 === n._state ? r.onFulfilled : r.onRejected;

              if (null !== e) {
                var t;

                try {
                  t = e(n._value);
                } catch (e) {
                  return void s(r.promise, e);
                }

                u(r.promise, t);
              } else (1 === n._state ? u : s)(r.promise, n._value);
            })) : n._deferreds.push(r);
          }

          function u(t, e) {
            try {
              if (e === t) throw new TypeError("A promise cannot be resolved with itself.");

              if (e && ("object" == _typeof(e) || "function" == typeof e)) {
                var n = e.then;
                if (e instanceof i) return t._state = 3, t._value = e, void c(t);
                if ("function" == typeof n) return void l((r = n, o = e, function () {
                  r.apply(o, arguments);
                }), t);
              }

              t._state = 1, t._value = e, c(t);
            } catch (e) {
              s(t, e);
            }

            var r, o;
          }

          function s(e, t) {
            e._state = 2, e._value = t, c(e);
          }

          function c(e) {
            2 === e._state && 0 === e._deferreds.length && i._immediateFn(function () {
              e._handled || i._unhandledRejectionFn(e._value);
            });

            for (var t = 0, n = e._deferreds.length; t < n; t++) {
              o(e, e._deferreds[t]);
            }

            e._deferreds = null;
          }

          function a(e, t, n) {
            this.onFulfilled = "function" == typeof e ? e : null, this.onRejected = "function" == typeof t ? t : null, this.promise = n;
          }

          function l(e, t) {
            var n = !1;

            try {
              e(function (e) {
                n || (n = !0, u(t, e));
              }, function (e) {
                n || (n = !0, s(t, e));
              });
            } catch (e) {
              if (n) return;
              n = !0, s(t, e);
            }
          }

          i.prototype["catch"] = function (e) {
            return this.then(null, e);
          }, i.prototype.then = function (e, t) {
            var n = new this.constructor(r);
            return o(this, new a(e, t, n)), n;
          }, i.prototype["finally"] = e, i.all = function (t) {
            return new i(function (r, o) {
              if (!t || void 0 === t.length) throw new TypeError("Promise.all accepts an array");
              var i = Array.prototype.slice.call(t);
              if (0 === i.length) return r([]);
              var u = i.length;

              function s(t, e) {
                try {
                  if (e && ("object" == _typeof(e) || "function" == typeof e)) {
                    var n = e.then;
                    if ("function" == typeof n) return void n.call(e, function (e) {
                      s(t, e);
                    }, o);
                  }

                  i[t] = e, 0 == --u && r(i);
                } catch (e) {
                  o(e);
                }
              }

              for (var e = 0; e < i.length; e++) {
                s(e, i[e]);
              }
            });
          }, i.resolve = function (t) {
            return t && "object" == _typeof(t) && t.constructor === i ? t : new i(function (e) {
              e(t);
            });
          }, i.reject = function (n) {
            return new i(function (e, t) {
              t(n);
            });
          }, i.race = function (o) {
            return new i(function (e, t) {
              for (var n = 0, r = o.length; n < r; n++) {
                o[n].then(e, t);
              }
            });
          }, i._immediateFn = "function" == typeof d && function (e) {
            d(e);
          } || function (e) {
            t(e, 0);
          }, i._unhandledRejectionFn = function (e) {
            "undefined" != typeof console && console && console.warn("Possible Unhandled Promise Rejection:", e);
          };

          var n = function () {
            if ("undefined" != typeof self) return self;
            if ("undefined" != typeof window) return window;
            if (void 0 !== f) return f;
            throw new Error("unable to locate global object");
          }();

          "Promise" in n ? n.Promise.prototype["finally"] || (n.Promise.prototype["finally"] = e) : n.Promise = i;
        })();
      }).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {}, e("timers").setImmediate);
    }, {
      timers: 3
    }],
    3: [function (c, e, a) {
      (function (e, t) {
        var r = c("process/browser.js").nextTick,
            n = Function.prototype.apply,
            o = Array.prototype.slice,
            i = {},
            u = 0;

        function s(e, t) {
          this._id = e, this._clearFn = t;
        }

        a.setTimeout = function () {
          return new s(n.call(setTimeout, window, arguments), clearTimeout);
        }, a.setInterval = function () {
          return new s(n.call(setInterval, window, arguments), clearInterval);
        }, a.clearTimeout = a.clearInterval = function (e) {
          e.close();
        }, s.prototype.unref = s.prototype.ref = function () {}, s.prototype.close = function () {
          this._clearFn.call(window, this._id);
        }, a.enroll = function (e, t) {
          clearTimeout(e._idleTimeoutId), e._idleTimeout = t;
        }, a.unenroll = function (e) {
          clearTimeout(e._idleTimeoutId), e._idleTimeout = -1;
        }, a._unrefActive = a.active = function (e) {
          clearTimeout(e._idleTimeoutId);
          var t = e._idleTimeout;
          0 <= t && (e._idleTimeoutId = setTimeout(function () {
            e._onTimeout && e._onTimeout();
          }, t));
        }, a.setImmediate = "function" == typeof e ? e : function (e) {
          var t = u++,
              n = !(arguments.length < 2) && o.call(arguments, 1);
          return i[t] = !0, r(function () {
            i[t] && (n ? e.apply(null, n) : e.call(null), a.clearImmediate(t));
          }), t;
        }, a.clearImmediate = "function" == typeof t ? t : function (e) {
          delete i[e];
        };
      }).call(this, c("timers").setImmediate, c("timers").clearImmediate);
    }, {
      "process/browser.js": 1,
      timers: 3
    }],
    4: [function (e, t, n) {
      "use strict";

      Object.defineProperty(n, "__esModule", {
        value: !0
      });
      var s = e("./models/EmailJSResponseStatus"),
          i = e("./services/ui/UI"),
          c = null,
          a = "https://api.emailjs.com";

      function l(o, i, u) {
        return void 0 === u && (u = {}), new Promise(function (n, r) {
          var e = new XMLHttpRequest();

          for (var t in e.addEventListener("load", function (e) {
            var t = new s.EmailJSResponseStatus(e.target);
            200 === t.status || "OK" === t.text ? n(t) : r(t);
          }), e.addEventListener("error", function (e) {
            r(new s.EmailJSResponseStatus(e.target));
          }), e.open("POST", o, !0), u) {
            e.setRequestHeader(t, u[t]);
          }

          e.send(i);
        });
      }

      n.init = function (e, t) {
        c = e, a = t || "https://api.emailjs.com";
      }, n.send = function (e, t, n, r) {
        var o,
            i,
            u = {
          lib_version: "2.3.2",
          user_id: r || c,
          service_id: e,
          template_id: t,
          template_params: (o = n, i = document.getElementById("g-recaptcha-response"), i && i.value && (o["g-recaptcha-response"] = i.value), i = null, o)
        };
        return l(a + "/api/v1.0/email/send", JSON.stringify(u), {
          "Content-type": "application/json"
        });
      }, n.sendForm = function (e, t, n, r) {
        if ("string" == typeof n && (n = document.querySelector(n)), !n || "FORM" !== n.nodeName) throw "Expected the HTML form element or the style selector of form";
        i.UI.progressState(n);
        var o = new FormData(n);
        return o.append("lib_version", "2.3.2"), o.append("service_id", e), o.append("template_id", t), o.append("user_id", r || c), l(a + "/api/v1.0/email/send-form", o).then(function (e) {
          return i.UI.successState(n), e;
        }, function (e) {
          return i.UI.errorState(n), Promise.reject(e);
        });
      };
    }, {
      "./models/EmailJSResponseStatus": 5,
      "./services/ui/UI": 6
    }],
    5: [function (e, t, n) {
      "use strict";

      Object.defineProperty(n, "__esModule", {
        value: !0
      });

      var r = function r(e) {
        this.status = e.status, this.text = e.responseText;
      };

      n.EmailJSResponseStatus = r;
    }, {}],
    6: [function (e, t, n) {
      "use strict";

      Object.defineProperty(n, "__esModule", {
        value: !0
      });

      var r = function () {
        function e() {}

        return e.clearAll = function (e) {
          e.classList.remove(this.PROGRESS), e.classList.remove(this.DONE), e.classList.remove(this.ERROR);
        }, e.progressState = function (e) {
          this.clearAll(e), e.classList.add(this.PROGRESS);
        }, e.successState = function (e) {
          e.classList.remove(this.PROGRESS), e.classList.add(this.DONE);
        }, e.errorState = function (e) {
          e.classList.remove(this.PROGRESS), e.classList.add(this.ERROR);
        }, e.PROGRESS = "emailjs-sending", e.DONE = "emailjs-success", e.ERROR = "emailjs-error", e;
      }();

      n.UI = r;
    }, {}]
  }, {}, [4, 2])(4);
});

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sendMail = void 0;

var emailjs = _interopRequireWildcard(require("./email.min"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

var sendMail = function sendMail() {
  var contForm = document.getElementById('contact-form'),
      name = document.getElementById('nameInput'),
      email = document.getElementById('emailInput'),
      phone = document.getElementById('telInput'),
      content = document.getElementById('contentInput');
  emailjs.init("user_PazhDT15rotdDZGc7UbQE");
  contForm.addEventListener('submit', function (e) {
    e.preventDefault();

    if (!name.value || !email.value || !phone.value || !content.value) {
      document.querySelector('.send-msg p:nth-child(1)').style.display = 'none';
      document.querySelector('.send-msg p:nth-child(2)').style.display = 'block';
    } else {
      emailjs.sendForm('gmail', 'vidakovic', contForm);
      document.querySelector('.send-msg p:nth-child(2)').style.display = 'none';
      document.querySelector('.send-msg p:nth-child(1)').style.display = 'block';
      name.value = "";
      email.value = "";
      phone.value = "";
      content.value = "";
      name.focus();
    }
  });
};

exports.sendMail = sendMail;

},{"./email.min":1}],3:[function(require,module,exports){
"use strict";

var _serviceView = require("./serviceView");

var _mailForm = require("./mailForm");

$(function () {
  $('.banner-cont .banner-title h1').show('slide', {
    direction: 'left'
  }, 2000);

  if (window.location.href.indexOf("service.html") > -1) {
    (0, _serviceView.renderService)();
  }

  (0, _mailForm.sendMail)();
});

},{"./mailForm":2,"./serviceView":4}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderService = void 0;
var serviceContent = document.querySelector('.service-content');
var markup = "<div class=\"container\">\n        \t\t\t<header class=\"row\">\n          \t\t\t\t<div class=\"col-lg-12\">\n            \t\t\t\t<h1 class=\"pg-title headline text-center\">%title%</h1>\n          \t\t\t\t</div>\n        \t\t\t</header>\n        \t\t\t<div class=\"row\">\n          \t\t\t\t<div class=\"col-md-6\">\n            \t\t\t\t<img src=\"%img%\" class=\"shadow-lg\">\n          \t\t\t\t</div>\n          \t\t\t\t<article class=\"col-md-6 mt-5 mt-md-0\">\n            \t\t\t\t<p>%content%</p>\n          \t\t\t\t</article>\n        \t\t\t</div>        \n      \t\t\t</div>";

var editHTML = function editHTML(ttl, img, cont) {
  var newHtml;
  newHtml = markup.replace('%title%', ttl);
  newHtml = newHtml.replace('%img%', img);
  newHtml = newHtml.replace('%content%', cont);
  serviceContent.insertAdjacentHTML('beforeend', newHtml);
};

var renderService = function renderService() {
  // Get service name from URL
  var id = window.location.hash.replace('#', ''); // Clear all content from SERVICE page

  serviceContent.innerHTML = '';

  var serviceElements = function serviceElements(title, image, content) {
    this.title = title;
    this.image = image;
    this.content = content;
  };

  var newElement; // Render service page if an ID exists

  if (id) {
    switch (id) {
      case 'ser1':
        newElement = new serviceElements('Restaurativna dentalna medicina', 'img/restaurativna_medicina.jpg', 'Dentalna protetika podrazumijeva nadomještanje izgubljenih zuba i mekih tkiva usne šupljine. Koncept dentalne protetike kreće se dvama smjerovima – mobilnom i fiksnom protetikom. Mobilna protetika temelji se na izradi nadomjestaka koji se mogu skidati (radi čišćenja), dok se jednom postavljeni fiksni nadomjesci više ne mogu skinuti. Kombinacija fiksnog i mobilnog rada naziva se kombinirani rad kod kojega je jedan dio mobilan te pričvršćen veznim elementima za fiksni dio cementiran na zube u ustima. Zajedno čine cjelinu koja služi žvakanju čvrsto stojeći u ustima. Mobilna protetika podrazumijeva proteze, odnosno nadomjestak dijela ili cijele denticije koja se vadi iz usta radi čišćenja. Fiksna protetika podrazumijeva krunice i most. Krunica ili navlaka ima funkciju očuvanja zubne krune kod zuba oštećenih karijesom ili lomom, a u slučajevima kada zubna ispuna nije dovoljna. Osim toga, ima i estetsku funkciju zbog toga što može korigirati boju, poziciju i rotaciju postojećeg zuba izrazito estetskim materijalom (keramikom). Most jest nadomjestak jednog ili više zuba prilikom čega se bruse susjedni zubi koji preuzimaju ulogu nosača međučlanova nadomještajući zube koji nedostaju.');
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser2':
        newElement = new serviceElements('Estetska dentalna medicina', 'img/estetska_medicina.jpg', 'Estetska dentalna medicina smjer je dentalne medicine koji primarno prednost daje estetici, no to nikako nije na štetu funkcije zuba (čvrstoća, trajnost, mogućnost žvakanja i slično). Podrazumijeva materijale koji u vrlo visokoj točnosti oponašaju boju i oblik prirodnih zuba. Dio estetske dentalne medicine su i djelomične keramičke krunice (ljuskice) koje pokrivaju vidljivi dio zuba uz minimalno brušenje, kao i potpune keramičke krunice koje u cijelosti oponašaju izgled prirodnog zuba odlično prianjajući uz zubno meso i ne ostavljajući nikakav rub na njemu.');
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser3':
        newElement = new serviceElements('Dječja dentalna medicina', 'img/djecja_dentalna_medicina.jpg', 'Dječja dentalna medicina obuhvaća djecu do 18 godina starosti. Preporučamo da prvi posjet bude već oko druge godine starosti, kako bi vaše dijete, osim kontrole zuba, bilo u mogućnosti izgraditi potpuno povjerenje u okruženje i našeg liječnika dentalne medicine. Vrlo je važno da djeca taj posjet prihvate kao igru te na taj način od najranije dobi usvoje pozitivan stav prema oralnoj higijeni.');
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser4':
        newElement = new serviceElements('Endodoncija', 'img/endodoncija.jpg', 'Endodoncija je grana dentalne medicine koja se bavi unutrašnjošću zuba, odnosno prostorom u kojem se nalazi zubni živac. Uzrok upale zubnog živca može biti lom krune zuba, veliki zubni ispun ili prodor karijesa do granice iritacije. Najčešći simptom je jak bol tijekom noći, no on može biti i slabiji ili u potpunosti izostati. Odumiranjem živca i njegovim postupnim raspadanjem, gomilaju se bakterije koje mogu infekciju prenijeti na kost i okolna tkiva. Svakim zanemarivanjem ovih simptoma liječenje postaje zahtjevnije, a samim time i dugotrajnije. Cilj endodontskih zahvata jest uklanjanje unutarnjeg tkiva, dezinfekcija inficiranog prostora te hermetičko ispunjavanje unutrašnjosti zuba kako bi se zatvorio put postojećim bakterijama. U našoj ordinaciji dentalne medicine radi se strojna obrada korijenskih kanala koja daje izvrsne rezultate te omogućava znatno kraći tretman uz potpunu bezbolnost.');
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser5':
        newElement = new serviceElements('Dentalna protetika', 'img/dentalna_protetika.jpg', 'Dentalna protetika podrazumijeva nadomještanje izgubljenih zuba i mekih tkiva usne šupljine. Koncept dentalne protetike kreće se dvama smjerovima – mobilnom i fiksnom protetikom. Mobilna protetika temelji se na izradi nadomjestaka koji se mogu skidati (radi čišćenja), dok se jednom postavljeni fiksni nadomjesci više ne mogu skinuti. Kombinacija fiksnog i mobilnog rada naziva se kombinirani rad kod kojega je jedan dio mobilan te pričvršćen veznim elementima za fiksni dio cementiran na zube u ustima. Zajedno čine cjelinu koja služi žvakanju čvrsto stojeći u ustima. Mobilna protetika podrazumijeva proteze, odnosno nadomjestak dijela ili cijele denticije koja se vadi iz usta radi čišćenja. Fiksna protetika podrazumijeva krunice i most. Krunica ili navlaka ima funkciju očuvanja zubne krune kod zuba oštećenih karijesom ili lomom, a u slučajevima kada zubna ispuna nije dovoljna. Osim toga, ima i estetsku funkciju zbog toga što može korigirati boju, poziciju i rotaciju postojećeg zuba izrazito estetskim materijalom (keramikom). Most jest nadomjestak jednog ili više zuba prilikom čega se bruse susjedni zubi koji preuzimaju ulogu nosača međučlanova nadomještajući zube koji nedostaju.');
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser6':
        newElement = new serviceElements('Oralna kirurgija', 'img/oralna_kirurgija.jpg', 'U našoj se ordinaciji dentalne medicine izvode sljedeći oralno kirurški postupci: vađenje zuba, obrada koštanog grebena, oblikovanje zubnog mesa te ugradnja implantata. Prije vađenja zuba, obavljaju se sve pripremne radnje (kontrola premedikacije), a samo vađenje radi se u stanju potpune bezbolnosti. Nakon što je zub izvađen obavljaju se sve potrebne radnje (ukoliko je potrebno šivanje, antibiotik ili nešto drugo). U ovoj se ordinaciji svi kirurški zahvati obavljaju pod strogo kontroliranim uvjetima sterilizacije.');
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser7':
        newElement = new serviceElements('Parodontologija', 'img/parodontologija.jpg', 'Parodontologija je grana dentalne medicine koja se bavi liječenjem parodontnih tkiva koje okružuju zube i omogućuju njihovu stabilnost u zubnom luku. Njezin su glavni predmet zubno meso (gingiva), sveza koja drži zub u kosti (parodontni ligament) te kost. Općepoznati izraz u narodu za bolest ispadanja zdravih zuba jest parodontoza (označava kronični parodontitis). Ona započinje upalom zubnog mesa (gingvitisom) koji se očituje njegovim krvarenjem, a slijedi povlačenje potpornog tkiva zuba s korijena zuba te njihova sveza popušta. Parodontoza je najčešće uzrokovana manjkom oralne higijene te nakupljenim kamencem. Zahvati koji se izvode u našoj ordinaciji su: čišćenje svih naslaga ultrazvučnim instrumentom (skidanje kamenca), pjeskarenje zuba, poliranje zuba, tretiranje kemijskim sredstvima, struganje i poliranje zubnih korijena te uklanjanje viška zubnog mesa (gingivektomija). Pri prvim znakovima koji upućuju na parodontozu izrazito je važno obaviti pregled kako bi se ona na vrijeme počela liječiti. Obavezno je pridržavati se redovitih propisanih kontrola svakih tri do šest mjeseci kako se stanje ne bi pogoršalo ili vratilo.');
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser8':
        newElement = new serviceElements('Rendgen', 'img/rendgen.jpg', 'Rendgenski je snimak danas standardno sredstvo dentalne medicine, kako u dijagnostici, tako i u kontroli terapije. U našoj se ordinaciji koristi digitalni rendgen čija su zračenja minimalna, a pacijent je prilikom snimanja prekriven zaštitnom olovnom pregačom. Rezultate intraoralnog i retroalveolarnih snimaka moguće je primiti u digitalnom obliku.');
        editHTML(newElement.title, newElement.image, newElement.content);
        break;

      case 'ser9':
        newElement = new serviceElements('Ortopan', 'img/ortopan.jpg', 'Ortopan je snimka koja reproducira cjelokupnu maksilofacijalnu regiju, uključujući i temporomandibularne zglobove (TMZ). Snimka može biti i u digitalnom formatu na CD (digitalni ortopan) za prikazivanje na zaslonu računala. Svi ortopani rade se digitalnom CCD tehnologijom i imaju visoku preciznost svih prikazanih struktura što omogućuje uočavanje više detalja i bolju dijagnostičku primjenu. Optimalna geometrija snimanja omogućuju precizan i neiskrivljen prikaz zuba gornje i donje čeljusti. Jasno se uočavaju patološki procesi oko svih korijena zuba, stanje kosti u slučajevima parodontitisa te karijesne lezije na zubima. Dijagnostički najvažnija snimka za pravovremeno otkrivanje bolesti zuba i okolnih struktura.');
        editHTML(newElement.title, newElement.image, newElement.content);
        break;
    }
  }
};

exports.renderService = renderService;

},{}]},{},[3]);
